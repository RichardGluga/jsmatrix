This javascript code creates the Matrix scrolling-green-code effect as an animated background for web.
The code runs well on most things I've tested it on, including mobile and tablet browsers. Basically,
anything that supports standard Javascript, the canvas element, and CSS2/3.

You can see a demo of this code on my site at gluga.com.

NOTE: I am NOT the original author of this code. I got it from a blog post by "Louis" at
http://tech.reboot.pro/showthread.php?tid=2659

ALL CREDITS SHOULD GO TO THE ORIGINAL AUTHOR for this nice bit of code.

